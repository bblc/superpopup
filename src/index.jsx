import React from 'react';
import { Manager, Reference, Popper } from 'react-popper';

import css from './style.sss';

const Popup = () => {
    return (
        <Manager>
            <Reference>
              {({ ref }) => (
                <div ref={ref}>
                    i'm super Popup sss
                </div>
              )}
            </Reference>
            <Popper placement="left">
              {({ ref, style, placement, arrowProps }) => (
                <div ref={ref} style={style} data-placement={placement}>
                  Popper element
                  <div ref={arrowProps.ref} style={arrowProps.style} />
                </div>
              )}
            </Popper>
          </Manager>
    );
};

export default Popup;
