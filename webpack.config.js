const webpack = require('webpack');
const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const PROJECT_DEPS = process.env.PROJECT_DEPS || __dirname;

const babelDefaults = {
    presets: [['env', {
        targets: { uglify: true },
        modules: false }],
        'react',
        'stage-2'
    ],
    plugins: ['transform-runtime', 'transform-export-extensions'],
    babelrc: false,
    filename: '[name].js',
};

const babelLoader = `babel-loader?${JSON.stringify(babelDefaults)}`;

const cssLoaderOpts = JSON.stringify({
    modules: true,
    importLoaders: 1,
    localIdentName: '[name]__[local]--[hash:base64:5]',
});

// const sssVars = require(`./sugarss/_vars.js`);
// const postcssLoaderOpts = JSON.stringify({
//     config: {
//         ctx: {
//             defaultVars: sssVars,
//         }
//     }
// });

const config = {
    entry: {
            //server js
            index: ['./index.js'],
        },
        output: {
            path: path.resolve(__dirname, 'build/'),
            filename: '[name].js',
            library: 'superpopup',
            libraryTarget: 'umd',
            umdNamedDefine: true
        },
        module: {
            rules: [
                { test: /\.jsx?$/, exclude: /(node_modules)/, use: babelLoader },
                {
                    test: /\.sss$/,
                    use: ExtractTextPlugin.extract({
                        fallback: 'style-loader',
                        use: [
                            { loader: 'css-loader', options: cssLoaderOpts },
                            { loader: 'postcss-loader' }
                        ]
                    })
                }
            ],
        },
        resolve: {
            modules: [
                path.resolve(__dirname, 'src'),
                path.resolve(PROJECT_DEPS, 'node_modules'),
                'node_modules'
            ],
            extensions: ['.js', '.jsx'],
        },
        resolveLoader: {
            modules: [path.resolve(PROJECT_DEPS, 'node_modules'), 'node_modules']
        },
        plugins: [
            new ExtractTextPlugin({ filename: 'css/[name].css', allChunks: true })
        ],
};

module.exports = config;
