/* eslint-disable import/no-commonjs,global-require */
const path = require('path');
const WORK_DIR = process.env.WORK_DIR || '/work';


function urlMapper(url, type) {
    var fontUrl = `/fonts/${url}`;
    if (url.includes('/fonts/')) {
        fontUrl = url;
    };

    var imgUrl = `/image/${url}`;
    if (url.includes('/image/')) {
        imgUrl = url;
    };

    switch (type) {
        case 'src':
            return fontUrl;
        default:
            return imgUrl;
    }
}


module.exports = ({ file, options, env }) => ({
    parser: file.extname === '.sss' ? 'sugarss' : false,
    plugins: options.plugins ? options.plugins : (
        env === 'production' ? [
            require('postcss-easy-import')({ extensions: ['.sss'] }),
            require('postcss-mixins'),
            require('postcss-each'),
            require('postcss-for'),
            require('postcss-variables')({ globals: options.defaultVars }),
            require('postcss-conditionals'),
            require('postcss-extend'),
            require('postcss-nested'),
            require('postcss-url-mapper')(urlMapper, { atRules: true }),
            require('postcss-automath'),
            require('postcss-color-function'),
            require('autoprefixer')({ browsers: ['last 3 versions'] }),
            require('postcss-discard-comments'),
            require('postcss-normalize-whitespace'),
            require('css-mqpacker')({ sort: true }),
        ] : [
                require('postcss-easy-import')({ extensions: ['.sss'] }),
                require('postcss-mixins'),
                require('postcss-each'),
                require('postcss-for'),
                require('postcss-variables')({ globals: options.defaultVars }),
                require('postcss-conditionals'),
                require('postcss-extend'),
                require('postcss-nested'),
                require('postcss-url-mapper')(urlMapper, { atRules: true }),
                require('postcss-automath'),
                require('postcss-color-function'),
                require('postcss-reporter'),
                require('autoprefixer')({ browsers: ['last 3 versions'] }),
                require('postcss-discard-comments'),
                require('postcss-normalize-whitespace'),
                require('css-mqpacker')({ sort: true }),
            ]
    )
});
